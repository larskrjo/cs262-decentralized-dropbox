package ddb;

import java.io.IOException;

import ddb.app.Application;
import ddb.discovery.Discover;
import ddb.discovery.MyMachine;
import ddb.system.BoxDir;
import ddb.system.SystemState;

public class Main {

    public static void main(String[] args) throws IOException {
    	if (args.length == 0) {
    		BoxDir.setBoxPath("");
    	} else {
    		BoxDir.setBoxPath(args[0]);
    	}
    	
    	MyMachine.touchMeFile();
    	Discover.touchDiscoveryFolder();
    	SystemState.touchMetaFile();
    	
    	Application app = new Application();
    	app.runDaemon();
    }

}
