package ddb.protocol;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

import ddb.discovery.Machine;
import ddb.system.AnnotatedFile;
import ddb.system.BoxDir;
import ddb.system.SystemState;
import ddb.util.NetworkUtil;
import ddb.util.ParseUtil;

/**
 * A session between two machines, either initialized by the client machine or
 * the server listening socket.
 */
public class Session {
	
	private static int timeoutMillis = 5000;
	
    private Socket socket;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    
    public Session(Machine machine) throws ConnectException, IOException {
    	this(machine.createSocket(timeoutMillis));
    }
    
    public Session(Socket socket) throws IOException {
        this.socket = socket;
        inputStream = new DataInputStream(socket.getInputStream());
        outputStream = new DataOutputStream(socket.getOutputStream());
    }
    
    /**
     * Send a packet to the session.
     * @param packet the packet to send
     * @throws IOException
     */
    public void sendPacket(Packet packet) throws IOException{
        NetworkUtil.sendPacket(outputStream, packet);
    }
    
    /**
     * Receive a packet from the session.
     * @return A packet
     * @throws IOException
     */
    public Packet receivePacket() throws IOException{
    	Packet packet = NetworkUtil.getPacket(inputStream);
    	return packet;
    }

    /**
     * Send the contents of a file to the session.
     * @param file the file whose contents should be sent
     * @throws IOException
     */
    public void sendFileContents(File file) throws IOException{
        NetworkUtil.sendFileContents(outputStream, file);
    }
    
    /**
	 * Receive a file's contents from the session.
	 * 
	 * @param annoFile
	 *            the file which we expect to receive the contents of
	 * @return the file data (in a temp file) and the annoFile in a
	 *         RequestFileResponse object
	 * @throws IOException
	 */
    public RequestFileResponse receiveFileContents(AnnotatedFile annoFile) throws IOException {
        return NetworkUtil.getFileContents(inputStream, annoFile);
    }
    
    /**
     * Send a file to the session.
     * @param file the file to send
     * @throws IOException
     */
    public void sendFile(AnnotatedFile file) throws IOException {
        Packet packet = new Packet(OpCode.I_HAVE_ANNOTATED_FILE, ParseUtil.toString(file).getBytes());
        sendPacket(packet);
        sendFileContents(new File(BoxDir.getPath(file.path)));
    }

    /**
     * Receive a file from the session.
     * @return The file received.
     * @throws IOException
     */
    public RequestFileResponse receiveFile() throws IOException {
		Packet packet = receivePacket();
		packet.expectHasHeader(OpCode.I_HAVE_ANNOTATED_FILE);
		AnnotatedFile annotatedFile = ParseUtil.toAnnotatedFile(new String(packet.getContent()));
		return receiveFileContents(annotatedFile);
    }
    
    /** 
     * Send a system state to the session.
     * @param state the system state to send
     * @throws IOException
     */
    public void sendSystemState(SystemState state) throws IOException {
        Packet packet = new Packet(OpCode.I_HAVE_SYSTEM_STATE, ParseUtil.toString(state).getBytes());
        sendPacket(packet);
    }
    
    /**
     * Receive a system state from the session.
     * @return The system state received.
     * @throws IOException
     */
    public SystemState receiveSystemState() throws IOException {
		Packet packet = receivePacket();
		packet.expectHasHeader(OpCode.I_HAVE_SYSTEM_STATE);
		return ParseUtil.toSystemState(new String(packet.getContent()));
    }
    
    /**
	 * Close the session. This must be called after every session creation to
	 * end the session.  Not closing a session is bad bad bad.
	 * 
	 * @throws IOException
	 */
    public void close() throws IOException {
    	outputStream.close();
    	inputStream.close();
    	socket.close();
    }
 
}
