package ddb.protocol;

/**
 * The space of possible messages sent between a client and server.
 */
public enum OpCode {

	/**
	 * Sent by a client to a server when it is doing a pull.
	 */
    GIVE_ME_SYSTEM_STATE("give_me_system_state"),
    
    /**
	 * Sent by a client to a server after it has done a diff between system
	 * states and needs to get raw file data for updated files.
	 */
    GIVE_ME_FILE("give_me_file"),
   
    /**
	 * Sent by a client to a server when the client has changes that the server
	 * should be aware of.
	 */
    DO_A_PULL("do_a_pull"),
    
    /**
	 * Sent by a server to a client in response to the GIVE_ME_SYSTEM_STATE
	 * opcode. Message should be accompanied by a SystemState object serialized
	 * as JSON.
	 */
    I_HAVE_SYSTEM_STATE("i_have_system_state"),
   
    /**
	 * Sent by a server to a client in response to the GIVE_ME_FILE opcode.
	 * Message should be accompanied by the raw bits of the file.
	 */
    I_HAVE_FILE("i_have_file"),
    
    /**
	 * Sent by a server to a client in response to the GIVE_ME_FILE opcode.
	 * Message should be accompanied by a SystemState object serialized as JSON.
	 */
    I_HAVE_ANNOTATED_FILE("i_have_annotated_file"),
    
    /**
     * Sent by a server to a client in response to a DO_A_PULL opcode.
     */
    I_AM_OKAY("i_am_okay"),
    
    /**
	 * Sent by a server to a client in response to any opcode. This should only
	 * be sent if there is a problem with the server or with the request.
	 */
    I_HAVE_PROBLEM("i_have_problem"),;
    
    String opcode;
    
    OpCode(String opcode) {
        this.opcode = opcode;
    }
    
}
