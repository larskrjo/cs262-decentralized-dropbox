package ddb.protocol;

/**
 * An abstraction over elements of the protocol that include an opcode and an
 * arbitrary payload.
 */
public class Packet {
    
    private OpCode opCode;
    private byte[] content;
    
    /**
     * @param the opcode for the packet
     * @param the packet payload
     */
    public Packet(OpCode opCode, byte[] content) {
    	assert(opCode != null);
    	assert(content != null);
        this.opCode = opCode;
        this.content = content;
    }
    
    public Packet(OpCode opCode) {
    	this(opCode, new byte[0]);
    }
    
    public OpCode getHeader() {
        return opCode;
    }
    
    public byte[] getContent() {
        return content;
    }
    
    /** throws an error if the packet does not have the header @code.
     * 
     * @param code the opcode to check
     * @return true (if it returns)
     */
    public boolean expectHasHeader(OpCode code) {
    	if (opCode == code) { return true; }
    	throw new Error("Unexpected Header");
	}
}
