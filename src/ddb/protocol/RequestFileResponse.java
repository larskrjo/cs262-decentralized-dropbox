package ddb.protocol;

import java.io.File;

import ddb.system.AnnotatedFile;

/**
 * A more structured representation of the response a server sends to a client
 * in response to a SEND_ME_FILE request.
 * 
 * The raw bits of the received file are redirected to a system temp file, which
 * this object contains.
 */
public class RequestFileResponse {
	
	File tempFile;
	AnnotatedFile annoFile;
    
    public RequestFileResponse(File tempFile, AnnotatedFile annoFile) {
        this.tempFile = tempFile;
        this.annoFile = annoFile;
    }
    
    public File getTempFile() { return tempFile; }
    public AnnotatedFile getAnnoFile() { return annoFile; }
}
