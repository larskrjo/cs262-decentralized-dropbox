package ddb.util;

public class TestUtil {
	/**
	 * This will fail if assertions are disabled, thus allowing a programatic
	 * way to ensure assertions are enabled before running tests.
	 */
	public static void failIfNoAssertions() {
		boolean assertions = false;
		try {
			assert(false);
		} catch (AssertionError e) { assertions = true; }
		if (!assertions) {
			throw new Error("assertions are off");
		}
	}
}
