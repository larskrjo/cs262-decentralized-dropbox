package ddb.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class HashUtil {

    /** 
     * @brief Hash a file and return a string representing its hex representation.
     * @param f: file
     * @return a String hash of @a f
     */
    public static String hash(File f) throws FileNotFoundException, IOException {
    	
    	byte[] buf = new byte[8192];
    	int len;
    	MessageDigest md;
    	try {
    		md = MessageDigest.getInstance("SHA");
    	} catch (NoSuchAlgorithmException ex) { throw new Error(ex); }

		while (true) {
			long fileLen = f.length();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) { /*meh*/ }
			if (f.length() == fileLen) {
				break;
			}
		}
	   
		md.reset();
		
		FileInputStream bin = new FileInputStream(f);
        try {
	        while ((len = bin.read(buf)) > -1) {
	            md.update(buf, 0, len);
	        }
	        byte[] hash = md.digest();
	        
	        return byteArray2Hex(hash);
    	} finally {
    		bin.close();
    	}
    }

    private static String byteArray2Hex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }
}
