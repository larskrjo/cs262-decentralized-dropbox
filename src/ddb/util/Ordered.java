package ddb.util;


/** An ordering on T objects. */
public interface Ordered<T> {
	
    public boolean lessThan(T other);
    
}
