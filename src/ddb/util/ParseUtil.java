package ddb.util;

import java.io.Reader;

import ddb.discovery.Machine;
import ddb.system.AnnotatedFile;
import ddb.system.SystemState;

/**
 * 
 * @class ParseUtil contains the necessary method to parse:
 * Machine, SystemState and Annotated file from JSON to Object
 * and from Object to JSON
 *
 */
public class ParseUtil {
    
    public static Machine toMachine(String s) {
        return GsonUtil.single.fromJson(s, Machine.class);
    }
    
    public static SystemState toSystemState(String s){
    	return GsonUtil.single.fromJson(s, SystemState.class);
    }
    
    public static SystemState toSystemState(Reader reader){
		return GsonUtil.single.fromJson(reader, SystemState.class);
    }
    
    public static <T> String toString(T t) {
        return GsonUtil.single.toJson(t);
    }

	public static AnnotatedFile toAnnotatedFile(String string) {
		return GsonUtil.single.fromJson(string, AnnotatedFile.class);
	}

}
