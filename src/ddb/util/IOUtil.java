package ddb.util;

import java.io.File;

public class IOUtil {
	/**
	 * @brief move @a f to @a dest
	 * @param f: a file
	 * @param dest: a destination file
	 */
	public static void moveTempFile(File f, File dest) {
		dest.getParentFile().mkdirs();
		f.renameTo(dest);
	}
	/**
	 * @brief move @a f to path @a path
	 * @param f: a file
	 * @param path: a path
	 */
	public static void moveTempFile(File f, String path) {
		moveTempFile(f, new File(path));
	}
	
	/**
	 * @brief deletes file @a f on the filesystem if exists
	 * @param f: a file
	 */
	public static void deleteFileIfExists(File f) {
		f.delete();
	}
	
	/**
	 * @brief deletes file at path @a path on the filesystem if exists
	 * @param path
	 */
	public static void deleteFileIfExists(String path) {
		deleteFileIfExists(new File(path));
	}

}
