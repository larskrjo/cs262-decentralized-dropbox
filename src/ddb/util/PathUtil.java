package ddb.util;

import org.apache.commons.lang3.StringUtils;

public class PathUtil {
	/**
	 * @param s1: string with format prefix+s1_rest where
	 * prefix may be empty
	 * @param s2: string with format prefix+s2_rest where
	 * prefix may be empty
	 * @return s1_rest
	 */
	public static String stripCommonPrefix(String s1, String s2) {
		int diffI = StringUtils.indexOfDifference(s1, s2);
		return StringUtils.substring(s1, diffI);
	}
}
