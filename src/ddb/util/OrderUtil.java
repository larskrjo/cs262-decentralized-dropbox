package ddb.util;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class OrderUtil {
	/**
	 * @brief Compare two maps m1 and m2 where m1 < m2 if: forall <k,v> in m1, v <=
	 * m2[k] AND there exists k' st. m1[k'] < m2[k']
	 * @return true if m1 < m2
	 */
	public static <K, V> boolean allLessOrEqOneStrictlyLess(
			Map<K, V> m1,
			Map<K, V> m2, 
			Comparator<V> ord, 
			V zerovalue) {
		boolean oneIsStrictlyLess = false;
		boolean allAreLessOrEq = true;
		Set<K> keyset = new HashSet<K>(m1.keySet());
		keyset.addAll(m2.keySet());
		for (K k : keyset) {
			V t1Val = (m1.containsKey(k)) ? m1.get(k) : zerovalue;
			V t2Val = (m2.containsKey(k)) ? m2.get(k) : zerovalue;
			if (ord.compare(t1Val, t2Val) < 0) {
				oneIsStrictlyLess = true;
			}
			if (ord.compare(t1Val, t2Val) > 0) {
				allAreLessOrEq = false;
			}
		}
		return oneIsStrictlyLess && allAreLessOrEq;
	}

	/**
	 * @return the usual comparator on long
	 */
	public static Comparator<Long> longComparator = new Comparator<Long>() {
		public int compare(Long o1, Long o2) { return o1.compareTo(o2); }
	};
	/**
	 * @return true if val1 < val2 (lexicographically),
	 * i.e. there exists  0<=k<max(val1.size,val2.size) such that
	 * for all i < k : val1[i] = val2[i]
	 * and val1[k] < val2[k]
	 */
	public static <K, V> boolean lexicographicallySmaller(
			TreeMap<K, V> val1,
			TreeMap<K, V> val2, 
			Comparator<V> ord, 
			V zerovalue) {
		TreeSet<K> keyset = new TreeSet<K>(val1.keySet());
		keyset.addAll(val2.keySet());
		for (K k : keyset) {
			V t1Val = (val1.containsKey(k)) ? val1.get(k) : zerovalue;
			V t2Val = (val2.containsKey(k)) ? val2.get(k) : zerovalue;
			if (ord.compare(t1Val, t2Val) < 0) {
				return true;
			}
			if (ord.compare(t1Val, t2Val) > 0) {
				return false;
			}
		}
		return false;
	}
}
