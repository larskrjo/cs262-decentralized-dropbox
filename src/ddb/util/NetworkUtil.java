package ddb.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

import ddb.protocol.OpCode;
import ddb.protocol.Packet;
import ddb.protocol.RequestFileResponse;
import ddb.system.AnnotatedFile;

public class NetworkUtil {
	
	public static final boolean packetDebug = false;

	public static final int N = 100;
	
	/**
	 * Send the raw data for a packet over a DataOutputStream
	 * @param out the output stream to send the bits
	 * @param packet the packet to send over the stream
	 * @throws IOException
	 */
	public static void sendPacket(DataOutputStream out, Packet packet) throws IOException {
		out.writeUTF(packet.getHeader().name());
		out.writeInt(packet.getContent().length);
		out.write(packet.getContent());
		if (packetDebug) {
			System.out.println("[PACKET SENT] \nheader: " + 
						       packet.getHeader() + 
						       "\ncontent: "+
						       new String(packet.getContent()));
		}
	}
	
	/**
	 * Receive the raw bits for a packet over a DataInputStream and piece it
	 * together into a Packet object.
	 * @param in the input stream to receive the bits
	 * @return the Packet sent over the wire
	 * @throws IOException
	 */
    public static Packet getPacket(DataInputStream in) throws IOException {
        String header = in.readUTF();
        int content_size = in.readInt();
        byte[] content = new byte[content_size];
        in.readFully(content, 0, content_size);
        
        if (packetDebug) {
        	System.out.println("[PACKET RECEIVED] \nheader: " + 
        					   header + 
        					   "\ncontent: "+new String(content));
        }
        return new Packet(OpCode.valueOf(header), content);
    }

    /**
     * Send the raw data for a file over a DataOutputStream.
     * @param out the output stream to send the bits
     * @param file the file whose contents should be sent
     * @throws IOException
     */
    public static void sendFileContents(DataOutputStream out, File file) throws IOException {
    	FileInputStream fileIn = null;
    	try {
    		fileIn = new FileInputStream(file);
    		IOUtils.copy(fileIn, out);
    	} finally {
    		if (fileIn != null) {
    			fileIn.close();
    		}
    	}
    }
    
    /**
	 * Receive the raw bits for a file over a DataInputStream and piece it
	 * together into a RequestFileResponse object.
	 * @param in the input stream to receive the bits
	 * @param annoFile the file we expect to be receiving
	 * @return a reference to the file data and file info sent over the wire
	 * @throws IOException
	 */
    public static RequestFileResponse getFileContents(DataInputStream in, AnnotatedFile annoFile) throws IOException {
    	File temp = File.createTempFile(new File(annoFile.path).getName(), "");
    	FileOutputStream tempOut = null;
    	try {
    		tempOut = new FileOutputStream(temp);
    		IOUtils.copy(in, tempOut);
    	} finally {
    		if (tempOut != null) {
    			tempOut.close();
    		}
    	}
    	
    	return new RequestFileResponse(temp, annoFile);
    }
}
