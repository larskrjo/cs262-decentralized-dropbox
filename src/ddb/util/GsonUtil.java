package ddb.util;

import com.google.gson.GsonBuilder;

/** A place to put a single, common instance of a Gson object. */
public class GsonUtil {
	public static com.google.gson.Gson single = new GsonBuilder().setPrettyPrinting().create();
}
