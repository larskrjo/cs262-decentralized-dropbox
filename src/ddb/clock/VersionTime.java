package ddb.clock;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import ddb.discovery.Machine;
import ddb.util.OrderUtil;
import ddb.util.Ordered;

/**
 * 
 * @class VersionTime represents a map of (key, value) pair
 * where each key is a machine and each value is the number of times
 * machine "key" has modified the associated file.
 *
 */
public class VersionTime implements Cloneable, Ordered<VersionTime> {
	
	public TreeMap<String, Long> vector;

	/**
	 * @brief creates an empty version time. Every Machine receives
	 * a default 0 value.
	 */
	public VersionTime() {
		this.vector = new TreeMap<String, Long>();
	}
	
	/**
	 * @param k a machine name
	 * @return a copy of this where the value for machine name @a k
	 * was incremented
	 */
	public VersionTime increment(String k) {
		VersionTime t = this.clone();
		Long v = t.vector.get(k);
		if (v == null) {
			t.vector.put(k, 1l);	
		} else {
			t.vector.put(k, v+1);
		}
		return t;
	}
	/**
	 * @param m a machine
	 * @return a copy of this where the value for machine @a m
	 * was incremented
	 */
	public VersionTime increment(Machine m) {
		return increment(m.getName());
	}
	
	/**
	 * @return true if this < other lexicographically
	 */
	public boolean lessThan(VersionTime other) {
		return OrderUtil.<String,Long>lexicographicallySmaller(vector, other.vector, OrderUtil.longComparator, 0l);
	}

	@SuppressWarnings("unchecked")
	@Override
	public VersionTime clone() {
		try {
			VersionTime ret = (VersionTime) super.clone();
			ret.vector = (TreeMap<String, Long>) vector.clone();
			return ret;
		} catch (CloneNotSupportedException ex) {
			throw new Error(ex);
		}
	}

	public String toString() {
		ArrayList<String> pieces = new ArrayList<String>();
		for (Entry<String, Long> e : vector.entrySet()) {
			pieces.add(e.getKey() + "=>" + e.getValue());
		}
		return StringUtils.join(pieces.toArray(), " | ");
	}
	
	public static void test() {
		VersionTime t1 = new VersionTime();
		VersionTime t2 = new VersionTime();
		assert(!t1.lessThan(t2));
		assert(!t2.lessThan(t1));
		VersionTime t3 = new VersionTime().increment("A");
		VersionTime t4 = new VersionTime().increment("B");
		VersionTime t5 = new VersionTime().increment("A").increment("B");
		assert(t1.lessThan(t3) && t1.lessThan(t4) && t1.lessThan(t5));
		assert(t4.lessThan(t3) && t3.lessThan(t5));
		assert(t4.lessThan(t5));		
	}
}
