package ddb;

import ddb.clock.VersionTime;
import ddb.discovery.Machine;
import ddb.system.AnnotatedFile;
import ddb.system.SystemDiff;
import ddb.system.SystemState;
import ddb.util.TestUtil;

public class Test {
	public static void main(String[] args) {
		TestUtil.failIfNoAssertions();
		VersionTime.test();
		Machine.test();
		AnnotatedFile.test();
		SystemDiff.test();
		SystemState.test();
		System.out.println("tests passed");
	}
}
