package ddb.app;

import java.io.IOException;
import java.net.ConnectException;

import ddb.discovery.Machine;
import ddb.discovery.MyMachine;
import ddb.protocol.OpCode;
import ddb.protocol.Packet;
import ddb.protocol.RequestFileResponse;
import ddb.protocol.Session;
import ddb.system.AnnotatedFile;
import ddb.system.SystemState;
import ddb.util.ParseUtil;

/**
 * An Application Client. Implements the client logic for the protocol.
 */
public class Client {
	
	Machine machine;
	
	/**
	 * Initiate client logic between another machine.
	 * @param machine
	 */
	public Client(Machine machine) {
		this.machine = machine;
	}

	/**
	 * Asks for a file from the other machine.
	 * @param reqAnnotatedFile the file to request.
	 * @return the response from the other machine.
	 * @throws IOException
	 */
	public RequestFileResponse requestFile(AnnotatedFile reqAnnotatedFile) throws IOException {
		Session session = null;
		try {
			session = new Session(machine);
			
			Packet packet = new Packet(OpCode.GIVE_ME_FILE, reqAnnotatedFile.path.getBytes());
			session.sendPacket(packet);
			RequestFileResponse response = session.receiveFile();
			
			return response;
		} finally  {
			if (session != null) {
				session.close();
			}
		}
	}

	/**
	 * Ask for the other machine's system state.
	 * @return the system state of the other machine.
	 * @throws ConnectException
	 * @throws IOException
	 */
	public SystemState requestSystemState() throws ConnectException, IOException {
		Session session = null;
		try {
			session = new Session(machine);
			
			Packet packet = new Packet(OpCode.GIVE_ME_SYSTEM_STATE);
			session.sendPacket(packet);
			SystemState state = session.receiveSystemState();
			
			return state;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/**
	 * Ask the other machine to perform a pull.
	 * @throws IOException
	 */
	public void requestPull() throws IOException {
		Session session = null;
		try {
			session = new Session(machine);

			Packet packet = new Packet(OpCode.DO_A_PULL, ParseUtil.toString(MyMachine.myMachine()).getBytes());
			session.sendPacket(packet);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
