package ddb.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import ddb.discovery.MyMachine;

/**
 * An application server. Spawns a new ServerConnectionHandler for incoming
 * connections.
 */
public class Server implements Runnable {
	
	Application app;
	boolean isReady;
	
	public Server(Application app) {
		this.app = app;
	}
	
	/**
	 * Wait for connections from clients and handle them by spawning a handler
	 * thread.
	 */
	public void run() {
		ServerSocket serverSocket = null;
    	try {
			serverSocket = new ServerSocket(MyMachine.myMachine().getPort());
			System.out.println("LISTENING on port: " + MyMachine.myMachine().getPort());
			synchronized (this) {
				isReady = true;
				notifyAll();
			}
			while (true) {
				Socket sock = serverSocket.accept();
				new Thread(new ServerConnectionHandler(app, sock)).start();
			}
    	} catch (IOException ex) { 
    		throw new Error(ex); 
    	} finally {
    		if (serverSocket != null) {
    			try {
    				serverSocket.close();
    			} catch (IOException ex) { throw new Error(ex); }
    		}
    	}
    	
    }
    
	/**
	 * isReadyToServe() will be true once the server is listening on a port. To
	 * wait for this to be true, another thread should acquire this object's
	 * lock and wait. This object will call notifyAll() when isReadyToServe()
	 * flips from false to true.
	 * 
	 * @return Whether or not the server is ready to serve clients.
	 */
    public boolean isReadyToServe() {
    	return isReady;
    }

}
