package ddb.app;

import java.io.IOException;

/**
 * 
 * @class MonitorChanges Runnable in charge of listening
 * to modification, addition or deletion inside the dropbox
 * and modify the system state accordingly
 *
 */
public class MonitorChanges implements Runnable {
	Application app;

	public MonitorChanges(Application app) {
		this.app = app;
	}
	
	public void run() {
		try {
			while (true) {
				boolean changed = app.updateLocalState();
				if (changed) {
					app.pushToPeers();
				} else {
				}
				Thread.sleep(5000);
			}
		} 
		catch (IOException ex) { throw new Error(ex); }
		catch (InterruptedException ex) { throw new Error(ex); }
	}


}
