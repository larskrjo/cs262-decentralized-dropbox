package ddb.app;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import ddb.discovery.Discover;
import ddb.discovery.Machine;
import ddb.protocol.RequestFileResponse;
import ddb.system.AnnotatedFile;
import ddb.system.BoxDir;
import ddb.system.SystemDiff;
import ddb.system.SystemState;
import ddb.util.IOUtil;

/**
 * 
 * @class Represents the execution of the application on a single
 * machine. Implements the high-level operations.
 *
 */

public class Application {
	
	public static boolean updateDebug = false;
	
	public static ReentrantLock lock = new ReentrantLock();
	
	/**
	 * @brief Default constructor
	 */
	public Application() {}

	/**
	 * @brief run the daemon that records local file modifications
	 * and trigger the push operations
	 * @throws IOException
	 */
	public void runDaemon() throws IOException {
        updateLocalState();
        pullFromPeers();
        Server s = new Server(this);
		// the server thread will call notify when it is ready. acquire the
		// server's lock to avoid race conditions
        synchronized(s) {
        	new Thread(s).start();
        	while (!s.isReadyToServe()) {
        		try {
        			s.wait();
        		} catch (InterruptedException ex) { throw new Error(ex); }
        	}
        }
        pushToPeers();
        MonitorChanges mc = new MonitorChanges(this);
        new Thread(mc).start();
    }

	/**
	 * @brief updates the current state in the .meta file from the
	 * current content of the dropbox
	 * @return true if the state needed to be updated
	 * @throws IOException
	 */
	public boolean updateLocalState() throws IOException {
		if (updateDebug) {
			System.out.println("UPDATING local state");
		}
		SystemState oldState = null, currentState = null;
		boolean updated = false;
		lock.lock();
		try {
			oldState = SystemState.stateFromMeta();
			currentState = SystemState.getCurrentState(oldState);
			SystemDiff diff = oldState.diff(currentState);
			if (updateDebug || diff.numberOfDifferences() > 1) {
				System.out.println("UPDATING changes: " + diff.numberOfDifferences());
			}
			if (diff.numberOfDifferences() != 0) {
				currentState.writeToMeta();
				updated = true;
			}
		} finally {
			lock.unlock();
		}

		return updated;
	}

	/**
	 * @brief Pull from the whole list of peers
	 * @throws IOException
	 */
    public void pullFromPeers() throws IOException {
        List<Machine> machines = Discover.getPeers();
        for(Machine machine: machines) {
        	pullFromPeer(machine);
        }        
    }
    
    /**
     * @brief Pull from machine @a machine. Compare the local system
     * state and the one from @a machine. Solve conflicts and updates
     * the state accordingly.
     * @param machine: another machine on the network
     * @throws IOException
     */
    public void pullFromPeer(Machine machine) throws IOException {
    	boolean altered = false;
    	
    	boolean succeeded = false;
    	while (!succeeded) {
    		System.out.println("PULLING from peer: " + machine);
    		try {
    			SystemState peerState = new Client(machine).requestSystemState();
    			lock.lock();
    			SystemState ourState = SystemState.stateFromMeta();
    			SystemDiff diff = ourState.diff(peerState);
    			System.out.println("PULLING changes: " + diff.numberOfDifferences());	
    			altered = resolveSystemDiff(ourState, diff, machine);
    			succeeded = true;
    		} catch (ConnectException ex) {
    			System.out.println("connection REFUSED from: " + machine);
    			succeeded = true;
    		} catch (SocketTimeoutException ex) {
    			System.out.println("connection TIMEOUT from: " + machine);
    		}
	    	if (!succeeded) {
		    	try {
		    		Thread.sleep(3000);
		    	} catch (InterruptedException ex2) { /*meh*/ }
	    	}
    	}
    	
    	if (altered) {
    		pushToPeers();
    	}
    }

    /**
     * @brief Push to the list of peers, i.e. ask them
     * to perform a pull.
     * 
     * @throws IOException
     */
    public void pushToPeers() throws IOException {
    	System.out.println("PUSHING to peers");
        List<Machine> machines = Discover.getPeers();
        for(Machine machine: machines) {
        	boolean succeeded = false;
        	while (!succeeded) {
	        	try {
	        		new Client(machine).requestPull();
	        		succeeded = true;
	        	} catch (ConnectException ex) {
	        		System.out.println("connection REFUSED from: " + machine);
	        		succeeded = true;
	    		} catch (SocketTimeoutException ex) {
	    			System.out.println("connection TIMEOUT from: " + machine);
	    			try {
	    				Thread.sleep(3000);
	    			} catch (InterruptedException ex2) { /*meh*/ }
	    		}
        	}
        }
    }
    
    /**
     * @brief Update the system state with every annotated file contained in
     * diff. Delete the non-alive file in diff. Updates the content of the files
     * with alive flag set to true
     * 
     * @param state: a system state
     * @param diff: a system diff
     * @param machine: a machine on the network
     * @return true if @a diff contains differences
     * @throws IOException
     */
    public boolean resolveSystemDiff(SystemState state, SystemDiff diff, Machine machine) throws IOException {
    	try {
	    	if (diff.numberOfDifferences() == 0) return false;
			
	    	for (AnnotatedFile annoFile : diff.getDeleteDiffs()) {
	    		AnnotatedFile myFile = state.findFileByPath(annoFile.path);
	    		if (myFile != null && myFile.alive) {
	    			IOUtil.deleteFileIfExists(BoxDir.getPath(annoFile.path));
	    		}
	    		state.updateAnnotatedFile(annoFile);
		 	}
	    
	    	state.writeToMeta();

    	} finally {
    		lock.unlock();
    	}
		

		for (AnnotatedFile annoFile : diff.getFetchDiffs()) {
			RequestFileResponse response = new Client(machine).requestFile(annoFile);
			lock.lock();
			try {
				state = SystemState.stateFromMeta();
				assert(response.getAnnoFile().path.equals(annoFile.path));
				updateSystemStateAdd(state, response.getAnnoFile(), response.getTempFile());
				state.writeToMeta();
			} finally {
				lock.unlock();
			}
		}
		return true;
    }

    /**
     * @param state: a system state
     * @param otherFile: an annotated file
     * @param tempFile: a file associated with content
     * @post updates the annotatated file with the same path as otherFile
     * with otherFile
     * @post move tempFile at path of anotated file otherFile
     */
	public void updateSystemStateAdd(SystemState state, AnnotatedFile otherFile, File tempFile) {
		AnnotatedFile myFile = state.findFileByPath(otherFile.path);
		
		if (myFile == null) {
			// my file doesn't exist, write it
			IOUtil.moveTempFile(tempFile, BoxDir.getPath(otherFile.path));
			state.updateAnnotatedFile(otherFile);
		} else if (myFile.version.lessThan(otherFile.version)) {
			// the incoming file dominates mine
			if (otherFile.alive) {
				// if the incoming file is alive, write it
				IOUtil.moveTempFile(tempFile, BoxDir.getPath(otherFile.path));
			} else {
				// the incoming file is dead, if mine is alive, remove it
				if (myFile.alive) {
					IOUtil.deleteFileIfExists(BoxDir.getPath(myFile.path));
				}
			}
			state.updateAnnotatedFile(otherFile);
		}
		
		IOUtil.deleteFileIfExists(tempFile);
	}	
}
