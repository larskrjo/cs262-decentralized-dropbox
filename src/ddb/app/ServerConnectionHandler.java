package ddb.app;

import java.io.IOException;
import java.net.Socket;

import ddb.discovery.Machine;
import ddb.protocol.Packet;
import ddb.protocol.Session;
import ddb.system.AnnotatedFile;
import ddb.system.SystemState;
import ddb.util.ParseUtil;

/**
 * Handles a single client connection.
 */
public class ServerConnectionHandler implements Runnable {

	Application app;
	Socket socket;

	public ServerConnectionHandler(Application app, Socket socket) {
		this.app = app;
		this.socket = socket;
	}

	/**
	 * Handle communication with the client over socket.
	 */
	public void run() {
		Session session = null;
		try {
			session = new Session(socket);
			Packet packet = session.receivePacket();
			Thread.currentThread().setName(packet.getHeader().name());
			switch (packet.getHeader()) {
			case GIVE_ME_FILE:
					String filename = new String(packet.getContent());
					AnnotatedFile annFile = SystemState.stateFromMeta().findFileByPath(filename);
					session.sendFile(annFile);
				break;
			case GIVE_ME_SYSTEM_STATE:
					SystemState state = SystemState.stateFromMeta();
					session.sendSystemState(state);
				break;
			case DO_A_PULL:
					session.close();
					session = null;
					Machine machine = ParseUtil.toMachine(new String(packet.getContent()));
					app.pullFromPeer(machine);
				break;
			default:
				throw new Error("Unknown opcode received by the server.");
			}
		} catch (IOException ex) { throw new Error(ex);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (IOException ex) { throw new Error(ex); }
			}
		}
	}
}
