package ddb.system;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import ddb.clock.VersionTime;
import ddb.discovery.MyMachine;
import ddb.util.GsonUtil;
import ddb.util.HashUtil;
import ddb.util.ParseUtil;
import ddb.util.PathUtil;

/**
 * 
 * @class SystemState represents the current state of the dropbox on 
 * the machine. More precisely, it contains a list of annotated files
 * and is written on disk into the .meta file 
 * 
 */

public class SystemState {
	
	public static Object metaFileLock = new Object();

	// list of files
	public final ArrayList<AnnotatedFile> files;

	public SystemState(ArrayList<AnnotatedFile> files) {
		this.files = files;
	}

	/* Static Methods */

	/** 
	 * @brief Conjure up a dummy, base system state. 
	 */
	public static SystemState getEmptyState() throws IOException {
		return getEmptyState(new File(BoxDir.getPath()));
	}
	
	/**
	 * @brief Creates a dropbox with the files contained in dir
	 * @param dir: location of the dropbox
	 * @return the system state of the dropbox with the files
	 * contained in dir. All version times associated to each file are incremented
	 * by 1.
	 * 
	 * @throws IOException if the folder could not be found
	 */
	public static SystemState getEmptyState(File dir) throws IOException {
		File[] files = dir.listFiles(new FileFilter() {
			public boolean accept(File f) { return !f.isHidden() && !f.isDirectory(); }
		});
		File[] subdirs = dir.listFiles(new FileFilter() {
			public boolean accept(File f) { return (f.getName().equals(".discovery") || !f.isHidden()) && f.isDirectory(); }
		});
		if (files == null || subdirs == null) {
			System.out.println("STRANGE MOMENT: " + dir.getCanonicalPath() + " is apparently not a directory");
			files = new File[0];
			subdirs = new File[0];
		}
		
		ArrayList<AnnotatedFile> annoFiles = new ArrayList<AnnotatedFile>();
		for (File f : files) {
			VersionTime version = new VersionTime().increment(MyMachine.myMachine());
			String fingerprint;
			try {
				fingerprint = HashUtil.hash(f);
			} catch (FileNotFoundException ex) {
				continue;
			}
			String relativePath = PathUtil.stripCommonPrefix(f.getCanonicalPath(), dir.getCanonicalPath()+"/");
			annoFiles.add(new AnnotatedFile(relativePath, version, fingerprint, true));
		}
		
		for (File subdir : subdirs) {
			SystemState dirState = getEmptyState(subdir);
			for (AnnotatedFile annoFile : dirState.files) {
				annoFiles.add(annoFile.addDirPrefix(subdir.getName()));
			}
		}
		
		return new SystemState(annoFiles);
	}

	/**
	 * @brief  Get the current system state
	 * @param oldState StateSystem object of the file system (should be based on the .meta file)
	 * @return ret the currentState with update hashes, and version
	 * 		   to be at least as up-to-date as s2.
	 */
	public static SystemState getCurrentState(SystemState oldState) throws FileNotFoundException, IOException {
		SystemState emptyState = getEmptyState();
		
		HashSet<String> allPaths = new HashSet<String>();
		
		for (AnnotatedFile annoFile : oldState.files) {
			allPaths.add(annoFile.path);
		}
		for (AnnotatedFile annoFile : emptyState.files) {
			allPaths.add(annoFile.path);
		}
		
		ArrayList<AnnotatedFile> resultFiles = new ArrayList<AnnotatedFile>();
		
		for (String path : allPaths) {
			AnnotatedFile oldAnnoFile = oldState.findFileByPath(path);
			AnnotatedFile emptyAnnoFile = emptyState.findFileByPath(path);
			assert (oldAnnoFile != emptyAnnoFile);
			
			if (oldAnnoFile == null) {
				// emptyAnnoFile == null
				// this is a new file
				resultFiles.add(emptyAnnoFile);
			} else if (emptyAnnoFile == null) {
				// oldAnnoFile == null
				if (oldAnnoFile.alive) {
					// this file was deleted
					resultFiles.add(oldAnnoFile.increment(MyMachine.myMachine(), "", false));
				} else {
					// oldAnnoFile is already marked as deleted. keep it around as is
					resultFiles.add(oldAnnoFile);
				}
			} else {
				// none are null
				// note it is impossible for emptyAnnoFile to be dead
				if (oldAnnoFile.alive) {
					// both are alive, check to see if it has been modified
					if (oldAnnoFile.fingerprint.equals(emptyAnnoFile.fingerprint)) {
						resultFiles.add(oldAnnoFile);
					} else {
						resultFiles.add(oldAnnoFile.increment(MyMachine.myMachine(), emptyAnnoFile.fingerprint, true));
					}
				} else {
					// oldAnnoFile is dead
					// it has been added
					resultFiles.add(oldAnnoFile.increment(MyMachine.myMachine(), emptyAnnoFile.fingerprint, true));
				}
			}
		}

		return new SystemState(resultFiles);
	}



	/**
	 * @return the system state previously stored in the .meta file
	 */
	public static SystemState stateFromMeta() throws IOException {
		synchronized (metaFileLock) {
			File metaDir = new File(BoxDir.getPath(".meta"));
			BufferedReader r = null;
			try {
				r = new BufferedReader(new FileReader(metaDir));
				SystemState state = ParseUtil.toSystemState(r);
				assert (state != null);
				return state;
			} finally {
				if (r != null) {
					r.close();
				}
			}
		}
	}
	
	/**
	 * @brief overwrites an empty state to an existing .meta file
	 * @throws IOException
	 */
    public static void replaceMetaWithEmpty() throws IOException {
        SystemState state = SystemState.getEmptyState();
        state.writeToMeta();
    }
    
    /**
     * @brief creates the .meta file and writes an empty state to it
     * @throws IOException
     */
    public static void touchMetaFile() throws IOException {
		boolean created = new File(BoxDir.getPath(".meta")).createNewFile();
		if (created) {
			replaceMetaWithEmpty();
		}
    }

	/**
	 * @brief Compute the unidirectionnal diff between this and other
	 * @param other a system state
	 * @return ret containing the list of files that this needs from other in order
	 * 		   to be at least as up-to-date as other.
	 */
	public SystemDiff diff(SystemState other) {
		ArrayList<AnnotatedFile> ret = new ArrayList<AnnotatedFile>();

		for (AnnotatedFile other_file : other.files) { 
			AnnotatedFile file = findFileByPath(other_file.path);
			if (file == null) {
				// other has other_file that s1 doesn't
				ret.add(other_file);
			} else {
				// compare the two files
				if (file.version.lessThan(other_file.version)) {
					// s1 needs other_file
					ret.add(other_file);
				}
			}
		}
		
		return new SystemDiff(ret);
	}


	/**
	 * @param query the query file
	 * @return the file in this having the same path as @a path
	 * 		   null if no such file exists
	 */
	public AnnotatedFile findFileByPath(String path) {
		for (AnnotatedFile f : files)
			if (f.path.equals(path))
				return f;
		return null;
	}

	/** 
	 * @brief Overwrites .meta to contain this system state. 
	 */
	public void writeToMeta() throws IOException {
		synchronized (metaFileLock) {
			File metaDir = new File(BoxDir.getPath(".meta"));
			FileWriter w = null;
			try {
				w = new FileWriter(metaDir);
		
				GsonUtil.single.toJson(this, w);
			} finally {
				if (w != null) {
					w.close();
				}
			}
		}
	}

	/**
	 * @brief adds/updates annoFile
	 * @param annoFile: the new version of the annotated file
	 * @post deletes the annotated file in the system state with
	 * the same path
	 * @post add annoFile to the system state
	 */
	public void updateAnnotatedFile(AnnotatedFile annoFile) {
		for (int i = 0; i < files.size(); i++) {
			if (files.get(i).path.equals(annoFile.path)) {
				files.remove(i);
				break;
			}
		}
		files.add(annoFile);
	}
	
	/**
	 * @brief unit testing method
	 */
	public static void test() {
		ArrayList<AnnotatedFile> files;
		
		files = new ArrayList<AnnotatedFile>();
		AnnotatedFile a1 = new AnnotatedFile("path1", new VersionTime().increment("B"), "fingerprint2", true);
		AnnotatedFile a2 = new AnnotatedFile("path2", new VersionTime().increment("B"), "fingerprint3", true);
		files.add(a1);
		files.add(a2);
		SystemState ss1 = new SystemState(files);
		
		files = new ArrayList<AnnotatedFile>();
		AnnotatedFile a3 = new AnnotatedFile("path1", new VersionTime(), "fingerprint1", true);
		AnnotatedFile a4 = new AnnotatedFile("path2", new VersionTime().increment("A"), "fingerprint2", false);
		files.add(a3);
		files.add(a4);
		SystemState ss2 = new SystemState(files);
		
		SystemDiff diff = ss1.diff(ss2);
		assert(diff.getDiffs().size() == 1);
		assert(diff.getDiffs().get(0) == a4);
	}

}
