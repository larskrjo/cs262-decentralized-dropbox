package ddb.system;

import java.util.ArrayList;

import ddb.clock.VersionTime;

/**
 * 
 * @class SystemDiff represents a directed difference between
 * two system states. A difference is defined by an AnnotatedFile.
 * 
 * A system diff between (u, v) contains the list of annotated file
 * that u needs from v in order to reach consistency.
 *
 */
public class SystemDiff {
	
	private ArrayList<AnnotatedFile> diffs;

	/**
	 * @brief Construct a system diff containing diffs as list
	 * of differences
	 * @param diffs
	 */
	public SystemDiff(ArrayList<AnnotatedFile> diffs) {
		this.diffs = diffs;
	}
	
	/**
	 * @return the number of differences in the system diff
	 */
	public int numberOfDifferences() {
		return diffs.size();
	}
	
	/**
	 * @return a String representation of the system diff
	 */
	public String toString() {
		StringBuffer ret = new StringBuffer();
		ret.append("System differences:\n");
		for (AnnotatedFile d : diffs)
			ret.append(d + "\n");
		ret.deleteCharAt(ret.length()-1);
		return ret.toString();
	}
	
	/**
	 * @return the list of differences in the system diff
	 */
	public ArrayList<AnnotatedFile> getDiffs() {
	    return diffs;
	}

	/**
	 * @return the list of deleted (or dead) files in the diff
	 */
	public ArrayList<AnnotatedFile> getDeleteDiffs() {
		ArrayList<AnnotatedFile> files = new ArrayList<AnnotatedFile>();
		for (AnnotatedFile annoFile : diffs) {
			if (!annoFile.alive) {
				files.add(annoFile);
			}
		}
		return files;
	}
	
	/**
	 * @return the list of modified files (or alive) in the diff
	 */
	public ArrayList<AnnotatedFile> getFetchDiffs() {
		ArrayList<AnnotatedFile> files = new ArrayList<AnnotatedFile>();
		for (AnnotatedFile annoFile : diffs) {
			if (annoFile.alive) {
				files.add(annoFile);
			}
		}
		return files;
	}
	
	/**
	 * @brief a testing function
	 */
	public static void test() {
		ArrayList<AnnotatedFile> files = new ArrayList<AnnotatedFile>();
		AnnotatedFile alive = new AnnotatedFile("path", new VersionTime(), "fingerprint", true);
		files.add(alive);
		AnnotatedFile dead = new AnnotatedFile("path", new VersionTime(), "fingerprint", false);
		files.add(dead);
		SystemDiff diff = new SystemDiff(files);
		assert(diff.getFetchDiffs().get(0) == alive);
		assert(diff.getDeleteDiffs().get(0) == dead);
	}
}
