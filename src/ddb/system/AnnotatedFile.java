package ddb.system;

import ddb.clock.VersionTime;
import ddb.discovery.Machine;


/** A file annotated with a timestamp, version time, and hash fingerprint. */
/**
 * 
 * @class AnnotatedFile represents the side information associated
 * with each file in the dropbox. More precisely, the annotaion defines:
 * - path: the path to the file from the dropbox
 * - version: the version time of the file
 * - fingerprint: a hash value of the content of the file
 * - alive: indicating if the file is associated with real content
 * 			i.e. if the file is alive or deleted
 *
 */
public class AnnotatedFile implements Cloneable {
	
	public String path;
	public VersionTime version;
	public String fingerprint;
	public boolean alive;
	
	/**
	 * @brief Construct a new annotated file with path @a path, version 
	 * @a version, fingerprint @a fingerprint and alive flag set to @a alive
	 * @param path
	 * @param version
	 * @param fingerprint
	 * @param alive
	 */
    public AnnotatedFile(String path, VersionTime version, String fingerprint, boolean alive) {
        this.path = path;
        this.version = version;
        this.fingerprint = fingerprint;
        this.alive = alive;
    }
    
    /**
     * @param dir a directory
     * @return a copy of this where the old path is set to
     * new_path = dir/old_path
     */
    public AnnotatedFile addDirPrefix(String dir) {
    	AnnotatedFile result = clone();
    	result.path = dir+"/"+path;
    	return result;
    }
	
    /**
     * @return a string representation of the annotated file
     */
    public String toString() {
    	return path;
    }
    
    /**
     * @brief Make a deep copy of the file
     */
    public AnnotatedFile clone() {
    	try {
    		AnnotatedFile res = (AnnotatedFile) super.clone();
    		res.version = version.clone();
    		return res;
    	} catch  (CloneNotSupportedException ex) { throw new Error(ex); }
    }
    
    /**
     * @param key
     * @param fingerprint
     * @param alive
     * @return a copy of this with a new fingerprint, alive flag and
     * an incremented version time for machine name @a key
     */
    public AnnotatedFile increment(String key, String fingerprint, boolean alive) {
    	return new AnnotatedFile(path, version.increment(key), fingerprint, alive);
    }

    /**
     * @param machine
     * @param fingerprint
     * @param alive
     * @return a copy of this with a new fingerprint, alive flag and
     * an incremented version time for machine @a machine
     */
	public AnnotatedFile increment(Machine machine, String fingerprint, boolean alive) {
		return new AnnotatedFile(path, version.increment(machine), fingerprint, alive);
	}
	
	/**
	 * @brief a testing method
	 */
	public static void test() {
		AnnotatedFile annoFile = new AnnotatedFile("path", new VersionTime(), "", true);
		AnnotatedFile prefix = annoFile.addDirPrefix("adir");
		assert(prefix.path.equals("adir/path"));
	}
}
