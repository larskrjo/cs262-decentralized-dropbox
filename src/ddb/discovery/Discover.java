package ddb.discovery;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import ddb.system.BoxDir;

/** Handles all requests and modifications
 * to the .discover folder that contains
 * the info of all machine's in the network
 * @class Discover
 *
 */
public class Discover {
	
	/**
	 * Creates the .discovery folder in the machine's box
	 * @throws IOException
	 */
	public static void touchDiscoveryFolder() throws IOException {
		boolean created = new File(BoxDir.getPath(".discovery")).mkdir();
		if (created) {
			updateMeEntry();
		}
	}
	
	/**
	 * Updates the current machine's file in the .discover folder
	 * Used upon startup incase the machine has changed
	 * ip addresses and port
	 * @throws IOException
	 */
	public static void updateMeEntry() throws IOException {
		addMachineToDiscovery(MyMachine.myMachine());
	}
	
	/**
	 * Retrieves a machine from the .discover folder 
	 * based on name
	 * @param name of machine that is requested
	 * @return Machine object that matches the desired name or null if does not exist
	 * @throws IOException
	 */
	public static Machine getMachineByName(String name) throws IOException {
		touchDiscoveryFolder();

		Machine machine = null;

		File[] discoveryFiles = new File(BoxDir.getPath(".discovery")).listFiles();
		for (File f : discoveryFiles) {
			if (f.getName().equals(name)) {
				machine = Machine.fromAddressString(name, StringUtils.chomp(FileUtils.readFileToString(f)));
				break;
			}
		}

		return machine;
	}
	
	/**
	 * Adds a machine to the discover folder. A machine's
	 * name is used as the file name, and its address and port
	 * as the file's contents
	 * @param m representing the machine to be added
	 * @throws IOException
	 */
	public static void addMachineToDiscovery(Machine m) throws IOException {
		touchDiscoveryFolder();		
		
		FileUtils.writeStringToFile(new File(BoxDir.getPath(".discovery/"+m.getName())), m.toAddressString()+"\n");
	}
	
	/**
	 * Reads in all the files in the .discovery folder, filters
	 * out the current machine and returns the remaining ones represented
	 * as machine objects
	 * @return arraylist containing machine objects of peers
	 * @throws IOException
	 */
	public static ArrayList<Machine> getPeers() throws IOException {
		touchDiscoveryFolder();
		
		ArrayList<Machine> result = new ArrayList<Machine>();
		
		File[] discoveryFiles = new File(BoxDir.getPath(".discovery")).listFiles();
		for (File f : discoveryFiles) {
			Machine m = Machine.fromAddressString(f.getName(), StringUtils.chomp(FileUtils.readFileToString(f)));
			if (!m.getName().equals(MyMachine.myMachine().getName())) {
				result.add(m);
			}
		}
		return result;
	}
}
