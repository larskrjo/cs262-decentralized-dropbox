package ddb.discovery;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * 
 * @class Machine object can represent either the current machine
 * or a peer. More precisely, the machine defines:
 * - name: UUID of the machine
 * - host: the ip address of the machine
 * - port: the port which the machine is listening to
 *
 */
public class Machine {

	private String name;
	private String host;
    private int port;	


    /**
     * @brief Creates a new Machine with UID @a name, hostname @a host
     * and using port @a port
     * @param name a UID
     * @param host a hostname
     * @param port a port number
     */
    public Machine(String name, String host, int port) {
    	this.name = name;
    	this.host = host;
    	this.port = port;
    }
    

    /**
     * @param name: the machine uid
     * @param s: the address string of format "address:port"
     * @return new machine from the address @a s
     */
    public static Machine fromAddressString(String name, String s) {
    	String host = s.substring(0, s.indexOf(':'));
    	int port = Integer.parseInt(s.substring(s.indexOf(':')+1));
    	return new Machine(name, host, port);
    }

    /**
     * @param s: a string of format "name:address:port"
     * @return new machine from the string @a s
     */
    public static Machine fromString(String s) {
    	String name = s.substring(0, s.indexOf(':'));
    	String rest = s.substring(s.indexOf(':')+1);
    	return fromAddressString(name, rest);
    }

    /**
     * @return the machine UID
     */
    public String getName() { return name; }
    /**
     * @return the machine hostname
     */
    public String getHost() { return host; }
    /**
     * @return the machine port number
     */
    public int getPort() { return port; }
    
    /**
     * @param timeoutMillis: timeout in milliseconds before giving up
     * @return a new socket on port with address this.getHostname(), this.getPort()
     * @throws IOException
     */
    public Socket createSocket(int timeoutMillis) throws IOException {
    	Socket s = new Socket();
    	s.setSoTimeout(timeoutMillis);
    	s.connect(new InetSocketAddress(host, port), timeoutMillis);
    	return s;
    }
    
    /**
     * @return a string representation of the machine object
     */
    public String toString() {
    	return name+":"+toAddressString();
    }
    
    /**
     * @return the address string associated with this
     */
    public String toAddressString() {
    	return host+":"+port;
    }
    
    /**
     * @brief Unit testing method
     */
    public static void test() {
    	Machine m = new Machine("name", "host", 101);
    	assert(m.toString().equals("name:host:101"));
    	Machine m2 = Machine.fromString(m.toString());
    	assert(m2.name.equals("name"));
    	assert(m2.host.equals("host"));
    	assert(m2.port == 101);
    }
}
