EMPTY =
SPACE = $(EMPTY) $(EMPTY)

JAVAC = javac
JAVACARGS = -Xlint
JAVA = java
JAVAARGS = -ea

SRC_DIR = src
BIN_DIR = bin
LIB_DIR = lib
SOURCES = $(wildcard $(SRC_DIR)/*/*.java) $(wildcard $(SRC_DIR)/*/*/*.java)
OBJECTS = $(subst $(SRC_DIR),$(BIN_DIR),$(SOURCES:.java=.class))
LIBS = $(wildcard $(LIB_DIR)/*/*.jar)
LIBS_COLON = $(subst $(SPACE),:,$(LIBS))

BOX = box

$(BIN_DIR):
	mkdir -p $(BIN_DIR)

build: $(BIN_DIR)
	$(JAVAC) $(JAVACARGS) -d $(BIN_DIR) -classpath "$(BIN_DIR):$(LIBS_COLON)" -sourcepath $(SRC_DIR) $(SOURCES)

run: build
	mkdir -p $(BOX)
	$(JAVA) -classpath '$(BIN_DIR):$(LIBS_COLON)' $(JAVAARGS) ddb.Main $(BOX) >> ./$(BOX)/.log &

test: build
	$(JAVA) -classpath "$(BIN_DIR):$(LIBS_COLON)" $(JAVAARGS) ddb.Test

clean:
	mkdir -p bin && rm -r bin

debug:
	echo $(BOX)

.DEFAULT_GOAL := build
.PHONY: run build clean debug
